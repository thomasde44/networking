# ping-ips


**ip_stuff.c** produces random ip values in the subnet of your choice and saves them in rand_ips.txt.                                         

**search.py** searches the log file where the random ips are stored, it then removes the duplicates and saves to a file new_rand_ips.txt.          

**ping_ips.sh**  then pings all the ips in the newly saved file new_rand_ips.txt.

