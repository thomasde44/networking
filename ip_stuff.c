#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <inttypes.h>d

int main() {
    FILE *fp;

    fp = fopen("./rand_ips.txt", "w+");
    int i, n, first_ip_digit, second_ip_digit;
    time_t t;
    n = 100;
    first_ip_digit = 129;
    second_ip_digit = 100;

    srand((unsigned) time(&t));
    char third[4];
    char fourth[4];
    char first[4];
    char second[4];
    
    for( i = 0 ; i < n ; i++ ) {
        unsigned int tmp = rand() % 32768;
        int a = tmp & 0xff00;
        int b = tmp & 0x00ff;

        sprintf(third, "%i", a >> 8); 
        sprintf(fourth, "%i", b); 
        sprintf(first, "%i", first_ip_digit);
        sprintf(second, "%i", second_ip_digit);

        fprintf(fp, "%s.%s.%s.%s\n", first, second, third, fourth);
    }

    return 0;
}
